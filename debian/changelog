libghemical (3.0.0-5) UNRELEASED; urgency=medium

  *

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Mon, 27 Oct 2014 20:37:36 +0100

libghemical (3.0.0-4) unstable; urgency=high

  * debian/patches/fix-underlinking.patch: Use -lmpi++ instead of -lmpi_cxx.
    Fixes FTBFS error on mpich architectures (s390x).

 -- Michael Banck <mbanck@debian.org>  Mon, 27 Oct 2014 20:27:35 +0100

libghemical (3.0.0-3) unstable; urgency=medium

  [ Daniel Leidert (dale) ]
  * debian/control (Uploaders): Removed myself.

  [ Michael Banck ]
  * Sync with Ubuntu (Closes: #765230).

  [ Graham Inggs ]
  * Add fix-underlinking.patch: link MPI libraries (Closes: #722162).
  * Add debian/clean: allow package to build twice in a row.

 -- Michael Banck <mbanck@debian.org>  Sun, 26 Oct 2014 15:18:26 +0100

libghemical (3.0.0-2ubuntu1) trusty; urgency=medium

  * Apply autoreconf for better port coverage.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Sun, 22 Dec 2013 03:18:07 +0000

libghemical (3.0.0-2build1) trusty; urgency=medium

  * No change rebuild against libopenmpi1.6.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Sun, 22 Dec 2013 02:37:55 +0000

libghemical (3.0.0-2) unstable; urgency=low

  * debian/libghemical5.symbols: Removed again. Because there is only one
    package depending on this library, the work necessary to maintain per-arch
    symbols files is not worth it. This will fix the FTBFS on many
    architectures.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 28 Nov 2011 19:36:11 +0100

libghemical (3.0.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Build-Depends): Dropped dpatch. Added autotools version.
    (Vcs-Browser): Point to the real location.
  * debian/libghemical5.symbols: Added symbols.
  * debian/rules: Dropped dpatch stuff.
  * debian/README.source: Dropped obsolete file.
  * debian/source/format: Added for source format 3.0 (quilt).
  * debian/patches/04_german_translation.dpatch: Dropped (applied upstream).
  * debian/patches/00list: Renamed to debian/patches/series and adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 27 Nov 2011 18:38:41 +0100

libghemical (2.99.1-2) unstable; urgency=low

  [ Daniel Leidert (dale) ]
  * debian/compat: Bumped to dh compat level 7.
  * debian/control (Build-Depends): Bumped dh version and dropped cdbs.
    (Standards-Version): Bumped to 3.8.4.
  * debian/rules: Dh 7 rewrite. Dropped cdbs.
  * debian/README.source: Dropped cdbs hint.

  [ Michael Banck ]
  * debian/patches/04_german_translation.dpatch: Added (closes: #637012).
    - po/de.po: Adds german translation by Chris Leick <c.leick@vollbio.de>.
    - po/LINGUAS: Adjusted.
  * debian/patches/00list: Adjusted.
  * debian/control (Standards-Version): Bumped to 3.9.2. 

 -- Michael Banck <mbanck@debian.org>  Mon, 05 Sep 2011 18:48:02 +0200

libghemical (2.99.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Transition from libghemical4 to libghemical5.
    (Standards-Version): Bumped to 3.8.3.
    (Build-Depends): Added intltool.
    (Vcs-Svn): Fixed vcs-field-uses-not-recommended-uri-format.
  * debian/libghemical4.install: Renamed to debian/libghemical5.install.
  * debian/libghemical-data.install: Added locale files.
  * debian/rules: Added rule to list missing files. Replaces --list-missing
    in DEB_DH_INSTALL_ARGS.
  * debian/patches/01_gcc43.dpatch: Dropped (applied upstream).
  * debian/patches/configure_libmopac_flags.dpatch: Ditto.
  * debian/patches/00list: Adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 22 Sep 2009 02:30:55 +0200

libghemical (2.98-2) unstable; urgency=low

  * Rebuild for unstable.

  * debian/control (Vcs-Svn): Set back to unstable location.
  * debian/copyright: Fixed license information (it's GPL2+).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 22 Feb 2009 00:35:20 +0100

libghemical (2.98-1) experimental; urgency=low

  * New upstream release 2.98.

  * debian/control: libghemical3gf to libghemical4 transition.
    (Build-Depends): Added mesa-common-dev. Increased mopac7 version to 1.14.
    (Uploaders): Removed LI Daobing. Thanks for your work!
    (Standards-Version): Bumped to 3.8.0.
    (Vcs-Svn): Set to experimental location.
    (Description): Small wording improvement.
    (Depends): Added missing libmopac7-dev for libghemical-dev. Added
    ${misc:Depends}.
    (Section): Fixed binary-control-field-duplicates-source.
  * debian/copyright: Updated and adjusted.
  * debian/libghemical3gf.install: Renamed to debian/libghemical4.install.
  * debian/libghemical-dev.install: Dropped libtool .la file.
  * debian/rules (DEB_DH_INSTALL_ARGS): List missing files.
  * debian/README.source: Added to be compliant to the policy.
  * debian/patches/01_gcc43.dpatch: Adjusted.
    - sasaeval.h, sasaeval.cpp: Fix new issues with GCC 4.3.
  * debian/patches/configure_libmopac_flags.dpatch: Added.
    - configure: Check for libmopac7 CFLAGS/LIBS instead for libglade-2.0.
  * debian/patches/00list: Adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 31 Jan 2009 16:53:44 +0100

libghemical (2.96-2) unstable; urgency=low

  [ LI Daobing ]
  * bump compat level to 5:
    - debian/compat: updated.
    - debian/control: depends on debhelper (>= 5)
  * use dpatch:
    - debian/rules: updated.
    - debian/control: build depends on dpatch.
  * FTBFS with GCC 4.3: missing #includes (Closes: #417336, #462696)
    - debian/patches/01_gcc43.dpatch: added.
    - debian/patches/00list: added.
  * bump standards version to 3.7.3:
    - debian/control: updated.
  * set source's section to science:
    - debian/control: updated.

  [ Daniel Leidert ]
  * debian/control: Added DM-Upload-Allowed for DM status.
    (Build-Depends): Fixed build-depends-on-1-revision lintian warning.
    s/refblas3-dev/libblas-dev and s/lapack3-dev/liblapack-dev transition,
    added gfortran and increased mopac7/mpqc dependency (closes: #465716).
    (Uploaders): Added myself.
    (Package, Depends, Conflicts, Replaces): libghemical3gf (gfortran)
    transition.
  * debian/copyright: Added copyright information (thanks to lintian).
  * debian/libghemical3.install: Renamed to debian/libghemical3gf.install for
    gfortran transition.
  * debian/rules: Install everything from debian/tmp.
    (DEB_CONFIGURE_EXTRA_FLAGS): gfortran transition (see #465716).
  * debian/libghemical3gf.install: Ditto.
  * debian/libghemical-data.install: Ditto.
  * debian/libghemical-dev.install: Ditto.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 10 Mar 2008 00:34:34 +0100

libghemical (2.96-1) unstable; urgency=low

  * New upstream release.

  [ Daniel Leidert ]
  * debian/control: Added Homepage and Vcs-* fields.
    (Standards-Version): Updated to latest version 3.7.2.
  * debian/watch: Added.

  [ LI Daobing ]
  * debian/control:
    - Source-Version is deprecated. Makes the package binNMU safe
      (closes: #435945).
    - add me to uploaders.
  * debian/copyright: new FSF address.

  [ Michael Banck ]
  * debian/control (Maintainer): Set to Debichem Team.
  * debian/control (Build-Depends): Bump libmopac7-dev dependency to 1.13-1.
  * Bump soname to libghemical.so.3:
    + debian/control (libghemical0c2a): Renamed to ...
    + debian/control (libghemical3): ... this.
    + debian/libghemical02ca.install: Renamed to ...
    + debian/libghemical3.install: ... this.

 -- LI Daobing <lidaobing@gmail.com>  Tue, 23 Oct 2007 23:48:23 +0800

libghemical (2.10-1) unstable; urgency=low

  * New upstream release.

 -- Michael Banck <mbanck@debian.org>  Tue, 12 Sep 2006 02:11:51 +0200

libghemical (2.00-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/02_mpqc_api_update.patch: Removed, no longer needed.
  * debian/control (Build-Depends): Removed libopenbabel-dev.
  * debian/patches/03_openbabel2.patch: Removed, no longer needed.
  * debian/patches/90_configure.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Wed, 10 May 2006 16:20:08 +0200

libghemical (1.91-2) unstable; urgency=low

  * debian/control (Build-Depends): Depend on openbabel-2.0.0.
    (Closes: #359895)
  * debian/patches/03_openbabel2.patch: New file, update configure.ac
    to look for openbabel-2.0.0.
  * debian/patches/90_configure.patch: New file, regenerated configure.

 -- Michael Banck <mbanck@debian.org>  Sat,  1 Apr 2006 16:01:24 +0200

libghemical (1.91-1) unstable; urgency=low

  * New upstream release.
  * Sync with Ubuntu.
  * debian/patches/01_geomopt.patch: Removed, no longer needed.
  * debian/patches/99_autotools.patch: Likewise.
  * debian/rules (Build-Depends): Add versioned Build-Depends on C++
    allocater transitioned versions of libopenbabel-dev and libsc-dev.
  * debian/rules (Build-Depends): Add versioned Build-Depends on
    libmopac7-dev 1.10-1.
  * debian/patches/02_mpqc_api_update.patch: New file, update for 
    mpqc-2.3.0 API changes.
  * debian/control (libghemical0): Renamed to ...
  * debian/control (libghemical0c2a): ... this package (Closes: #339204)
  * debian/control (libghemical0c2a): Conflict/Replace libghemical0.
  * debian/control (libghemical-dev): Update Depends to libghemical0c2a.
  * debian/libghemical0.install: Renamed to ...
  * debian/libghemical0c2a.install: ... this.

 -- Michael Banck <mbanck@debian.org>  Wed, 16 Nov 2005 00:59:37 +0100

libghemical (1.90-1ubuntu1) breezy; urgency=low

  * Added MOPAC7 support

 -- Jordan Mantha <mantha@lambda.chem.unr.edu>  Fri, 16 Sep 2005 22:04:04 -0700

libghemical (1.90-1) unstable; urgency=low

  * Initial Release.

 -- Michael Banck <mbanck@debian.org>  Tue, 19 Jul 2005 22:08:15 +0200

